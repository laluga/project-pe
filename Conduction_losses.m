function [Ploss_SiD, Ploss_SiCD, Ploss_SiMOSFET, Ploss_SiCMOSFET] = conduction_losses(I)
    %conduction losses
    % Ploss_Diode = U*I + I^2*R = Spannungsquelle + Widerstand
    %DIODE Si
    R_SiDiode = 0.125;
    U_SiDiode = [0.925, 1.18, 1.375, 1.5, 1.675];
    Ploss_SiD = (U_SiDiode.*I + I.^2*R_SiDiode);
    %DIODE SiC
    R_SiCDiode = 0.0282;
    U_SiCDiode = 0.8801;
    Ploss_SiCD = (U_SiCDiode*I + I.^2*R_SiCDiode);


    %Ploss_MOSFET = I^2*R
    %MOSFET Si
    R_SiMOSFET = 0.131;
    Ploss_SiMOSFET = I.^2*R_SiMOSFET;

    %MOSFET SiC
    R_SiCMOSFET = 0.076;
    Ploss_SiCMOSFET = I.^2*R_SiCMOSFET;
end