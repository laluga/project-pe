%Losses at Ls 1A, 3A, 5A,7A, 10A
Current_Vector = [1,3,5,7,10];
% Iavg = 2*Current_Vector./pi;
f_sw = 20000;
%%
%Conduction losses
[PlossCond_SiD, PlossCond_SiCD, PlossCond_SiMOSFET, PlossCond_SiCMOSFET] = Conduction_losses(Current_Vector);
%%
%Switching losses
[PlossSwitch_SiFET_SiD, PlossSwitch_SiFET_SiCD, PlossSwitch_SiCFET_SiD, PlossSwitch_SiCFET_SiCD] = Switching_losses(f_sw);
%Inductor losses
PlossInd = Inductor_losses(f_sw);
%%
%Total losses = switching losses + conduction losses
Ploss_SiFET_SiD = PlossSwitch_SiFET_SiD + PlossCond_SiD + PlossCond_SiMOSFET;
Ploss_SiFET_SiCD = PlossSwitch_SiFET_SiCD + PlossCond_SiCD + PlossCond_SiMOSFET;
Ploss_SiCFET_SiD = PlossSwitch_SiCFET_SiD + PlossCond_SiD + PlossCond_SiCMOSFET;
Ploss_SiCFET_SiCD = PlossSwitch_SiCFET_SiCD + PlossCond_SiCD + PlossCond_SiCMOSFET;

%Plot total losses 
%PLOT switching losses
plot(Current_Vector, Ploss_SiFET_SiD, 'LineWidth', 2);
hold on;
plot(Current_Vector, Ploss_SiFET_SiCD, 'LineWidth', 2);
plot(Current_Vector, Ploss_SiCFET_SiD, 'LineWidth', 2);
plot(Current_Vector, Ploss_SiCFET_SiCD, 'LineWidth', 2);
set(gca,'FontSize',14);
hold off;
legend('SiFET-SiD','SiFET-SiCD','SiCFET-SiD','SiCFET-SiCD', 'FontSize', 14, 'Location', 'northwest');
title('Total losses different technologies', 'FontSize', 14);
ylabel('Losses in W', 'FontSize', 16);
xlabel('Current in A', 'FontSize', 16);

% %PLOT switching losses
% plot(Current_Vector, PlossSwitch_SiFET_SiD, 'LineWidth', 2);
% hold on;
% plot(Current_Vector, PlossSwitch_SiFET_SiCD, 'LineWidth', 2);
% plot(Current_Vector, PlossSwitch_SiCFET_SiD, 'LineWidth', 2);
% plot(Current_Vector, PlossSwitch_SiCFET_SiCD, 'LineWidth', 2);
% set(gca,'FontSize',14);
% hold off;
% legend('SiFET-SiD','SiFET-SiCD','SiCFET-SiD','SiCFET-SiCD', 'FontSize', 14, 'Location', 'northwest');
% title('Switching losses in different technologies', 'FontSize', 14);
% ylabel('Losses in uJ', 'FontSize', 16);
% xlabel('Current in A', 'FontSize', 16);
