function [Ptot] = Inductor_losses(f_sw)

    if nargin == 0
        f_sw = 20000;
    end
    %Step 1 (Inputs)
    % Given values
    Vd = 385; %[V]
    Id = 10; %[A]
    Vrms = 230; %[V]
    Iripple = 1; %[A]
    
    % Necessary constants
    kcu = 0.3; %litz wires --> No skin effect 
    % Eddy currents are zero when using 3F3 ferrite
    my0 = 4*pi*1e-7;
    Ng = 4; %Number of airgaps double E core
    rho_cu = 2.2*1e-8; %Ohm/m^3
    
    % With the equation of Pin = Pout, it is possible to derive the RMS
    % current through the inductor   
    Irms = Vd*Id/Vrms; %[A]
    
    % We assume that the 100 Hz is in comparison to the switching frequency
    % nearly DC, therefore the maximum DC currrent through the inductor is
    % sqrt(2)* RMS value
    Idcmax = sqrt(2)*Irms; % %[A]
    Imax = Idcmax + Iripple/2;

    % In order to get most of the inductor we want to operate it near the
    % saturation but never inside the saturation. Therefore a safety margin
    % of 10 % is used. In saturation we would need a really high current in
    % order to increase the flux --> bad design 
    
    Bpeak = 310*1e-3; %in T --> saturation - 10% safety margin
    
    % With Bpeak we can now calculate Bac which is depending on the current
    % ripple in comparison to the maximum current. 
    Bac = Bpeak * (Imax-Idcmax)/Imax; % [T] % TBH I am still unsure about this value
    
    % Calculation of inductance which is dependent on the maximum current
    % ripple 
    L = Vd/(4*f_sw*Iripple);
    
    % Step 2 Calculate the stored energy inside the inductor
    E = L*Irms*sqrt(2)*Irms;
    
    % Deriving a value for Jrms, since later Jrms is squared proportional
    % to the losses we need to minimize
    % TODO: find out if we can change Jrms and what happens to the losses
    
    % in the book Acu is set to 0.64 mm^2 --> 0.64 * 10^-6 m^2
    % But this is way too low, therefore I will assume J = 6 A/mm^2 
%     Acu = 0.64 * 10^-6;
%     Jrms = Irms/Acu;
    Jrms = 6*10^6; % [A/m^2]
    Acu = Irms/Jrms; % m^2
    
    % Step 3 (core material, shape, size) the inital value is chosen to be
    % the following, 
    a = nthroot((E/(kcu*Jrms*Bpeak*2.1)),4);
    
    iCounter = 1;
    while true            
        Aw = 1.4*a^2;
        Acore = 1.5*a^2;
        d = 1.5*a; %Mohan

        %Step 7 (Winding parameters: kcu, Acu, Jrms, N)
        N = floor(kcu*Aw/Acu); 

        %Step 8 (Lmax)
        Lmax = (N*Acore*Bpeak)/(Irms*sqrt(2));

        %Step 10
        if Lmax > L
            break;
        else
            if iCounter == 1000
                error("Infinite loop");
            end
            a = a*1.01;
        end
        iCounter = iCounter + 1;
    end

    %Step 9 (Airgap length)
    lg_sum = Acore/((Acore*Bpeak)/(my0*N*sqrt(2)*Irms) - (a+d)/Ng); %in Si-units --> m
    lg = lg_sum/4;
    %% Power losses 
    Vcore = 13.5*a^3;
    Vwinding = 12.3*a^3;

    %specific power loss 
    % Watch out for the units. f_sw shall be in kHz, Bac in mT
    Psp = 1.5*1e-6*(f_sw*1e-3)^1.3*(Bac*1e3)^2.5; % [mW/cm^3]
    Psp = Psp*10^3; % [W/m^3]
    Psp_volumetric = Psp*Vcore; % [W]    

    %Copper winding losses
    Pw_qm = rho_cu*kcu*Jrms^2; %bezogen auf winding vol wegen kcu
    Pw_vol = Pw_qm*Vwinding; 

    Ptot = Pw_vol + Psp_volumetric;
end


%% Questions to ask:
% How are we supposed to choose Jrms? 
% 
