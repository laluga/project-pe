i = 1;
Current_Vector = [1,3,5,7,10];
%Conduction losses
[PlossCond_SiD, PlossCond_SiCD, PlossCond_SiMOSFET, PlossCond_SiCMOSFET] = Conduction_losses(Current_Vector);
for f_sw = 20000:10000:50000
    Frequency(i) = f_sw;
    %Switching losses
    [PlossSwitch_SiFET_SiD, PlossSwitch_SiFET_SiCD, PlossSwitch_SiCFET_SiD, PlossSwitch_SiCFET_SiCD] = Switching_losses(f_sw);
    %Inductor losses
    PlossInd = Inductor_losses(f_sw);
    %Total losses at 10A
    Ptot_SiFET_SiD(i) = PlossInd + PlossSwitch_SiFET_SiD(end) + PlossCond_SiD(end);
    Ptot_SiFET_SiCD(i) = PlossInd + PlossSwitch_SiFET_SiCD(end) + PlossCond_SiCD(end);
    Ptot_SiCFET_SiD(i) = PlossInd + PlossSwitch_SiCFET_SiD(end) + PlossCond_SiMOSFET(end);
    Ptot_SiCFET_SiCD(i) = PlossInd + PlossSwitch_SiCFET_SiCD(end) + PlossCond_SiCMOSFET(end);
    i = i + 1;
end

%%
%PLOT
plot(Frequency, Ptot_SiFET_SiD, 'LineWidth', 2);
hold on;
plot(Frequency, Ptot_SiFET_SiCD, 'LineWidth', 2);
plot(Frequency, Ptot_SiCFET_SiD, 'LineWidth', 2);
plot(Frequency, Ptot_SiCFET_SiCD, 'LineWidth', 2);
set(gca,'FontSize',14);
hold off;
legend('SiFET-SiD','SiFET-SiCD','SiCFET-SiD','SiCFET-SiCD', 'FontSize', 14, 'Location', 'northwest');
title('Total losses different technologies', 'FontSize', 14);
ylabel('Losses in W', 'FontSize', 16);
xlabel('Frequency in Hz', 'FontSize', 16);



