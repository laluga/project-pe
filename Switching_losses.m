function [PlossSwitch_SiFET_SiD, PlossSwitch_SiFET_SiCD, PlossSwitch_SiCFET_SiD, PlossSwitch_SiCFET_SiCD] = Switching_losses(f_sw)    
    %MicroJoule correction
    PlossSwitch_SiFET_SiD = [22.68,55.63,91.64,124.75,182.61]*f_sw*1e-6;
    PlossSwitch_SiFET_SiCD = [34.68,41.2,54.65,70.95,103.75]*f_sw*1e-6;
    PlossSwitch_SiCFET_SiD = [29.82,64.55,109.24,156.09,245.69]*f_sw*1e-6;
    PlossSwitch_SiCFET_SiCD = [43.04,50.97,67.95,90.44,138.53]*f_sw*1e-6;
end